/**
 *  @file SplitIntegerIntoCharacters.c
 *  @brief Defines the entry point for the console application and all
 *        additional private functions used in this project.
 *
 *  @author Vincent J. Palodichuk
 *  @bug No known bugs.
 */


#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include "SplitIntegerIntoCharacters.h"

const long MIN_INPUT_INTEGER = 1;
const long MAX_INPUT_INTEGER = 32767;
const char* SPACES_BETWEEN_CHARACTERS = "  ";
const long MAX_INVALID_INPUT_ATTEMPTS = 5;
const char* OUT_OF_MEMORY = "\nUnable to allocate memory.\n";
const char* PROMPT_MESSAGE = "Please enter an integer in the range of 1" \
					" - 32,767 inclusive (-1 or Q to quit): ";
const char* INVALID_INPUT_MESSAGE = "\nI did not recognize what you entered" \
					" as being an integer in the range of 1 - 32,767 inclusive.\n\n";
const char* OUTPUT_MESSAGE = "\nHere are the digits of the number you entered: \n";
const char* GOODBYE_MESSAGE = "\nThank you for taking the time to visit." \
					" I look forward to seeing you again soon.\n";
const char* TOO_MANY_TRIES = "\nThe maximum number of invalid inputs has been exeeded.\n";
const char* EMPTY_OUTPUT_MESSAGE = "Nothing\n";
const char* OUTPUT_FORMAT_MESSAGE = "%d";
const char* INPUT_FORMAT_MESSAGE = "%ld";
const char* END_OF_OUTPUT_MESSAGE = "\n\n";
const long BASE = 10;
const int QUIT_VAL = -1;
const int OK_RESULT = 0;
const int ERRORS_RESULT = 1;
const int OUT_OF_MEMORY_RESULT = 2;

/**
 *  @brief A private function that allocates and initializes the digits array.
 *         The caller is responsible for freeing the allocated memory.
 *
 *  @param num_digits the number of digits that the array needs to hold
 *  @param digits the address of the pointer that will be used to access the array
 *
 *  @return 0 if successfull; ortherwise non-zero to indicate an error.
 */
static int allocate_digits(size_t num_digits, long** digits);

/**
 *  @brief The main entry point into our program
 *
 *  @return 0 for success; otherwise non-zero to indicate an error.
 */
int main()
{
	int done = 0;
	long int_to_convert = 0;
	long invalid_input_count = 0;
	int result = OK_RESULT;
	long num_digits = 0;
	long *digits = NULL;

	while (!done) {
		printf(PROMPT_MESSAGE);
		int_to_convert = read_integer_from_stdin();

		if (int_to_convert >= MIN_INPUT_INTEGER && int_to_convert <= MAX_INPUT_INTEGER) {
			invalid_input_count = 0;

			num_digits = get_number_of_digits_in_base10_integer(int_to_convert);

			// Allocate enough memory to hold the number of digits in the number
			if (allocate_digits(num_digits, &digits)) {
				printf(OUT_OF_MEMORY);

				done = 1;
				result = OUT_OF_MEMORY_RESULT;
				continue;
			}

			get_digits(int_to_convert, num_digits, digits, num_digits);
			
			print_output_message(digits, num_digits);

			// Always clean-up
			free(digits);
			digits = NULL;
			int_to_convert = 0;
			num_digits = 0;
		}
		else if (int_to_convert == QUIT_VAL)
		{
			done = 1;
		} else 
		{
			printf(INVALID_INPUT_MESSAGE);

			if (++invalid_input_count > MAX_INVALID_INPUT_ATTEMPTS) {
				printf(TOO_MANY_TRIES);
				done = 1;
				result = ERRORS_RESULT;
			}
		}
	}

	printf(GOODBYE_MESSAGE);

    return result;
}

long get_integer_part_of_quotient(long numerator, long denominator)
{
	long result = 0;

	if (denominator != 0) {
		result = numerator / denominator;
	}

	return result;
}

long get_remainder_part_of_quotient(long numerator, long denominator)
{
	long result = 0;

	if (denominator != 0) {
		result = numerator % denominator;
	}

	return result;
}

long get_number_of_digits_in_base10_integer(long number)
{
	return (long)(floor(log10(number))) + 1;
}

long read_integer_from_stdin()
{
	int done = 0;
	long result = 0;
	long input = 0;
	char input_char = 'Q';

	result = scanf_s(INPUT_FORMAT_MESSAGE, &input);

	if (result == EOF || result == 0) {
		input = 0;

		if (result == 0) {
			input_char = fgetc(stdin);

			switch (input_char) {
				case 'Q':
				case 'q':
					input = QUIT_VAL;
					break;
			}
		}
	}

	// Clear out any buffered input
	while (input_char = fgetc(stdin) != '\n' && input_char != EOF);

	return input;
}

void print_output_message(long *output, size_t size)
{
	size_t i = 0;
	
	printf(OUTPUT_MESSAGE);

	if (output == NULL || size == 0) {
		printf(EMPTY_OUTPUT_MESSAGE);
	}
	else {
		for (i = 0; i < size; i++) {
			printf(OUTPUT_FORMAT_MESSAGE, output[i]);

			if (i + 1 < size) {
				printf(SPACES_BETWEEN_CHARACTERS);
			}
		}

		printf(END_OF_OUTPUT_MESSAGE);
	}
}

int get_digits(long number, size_t num_digits, long * digits, size_t array_length)
{
	int result = num_digits;
	size_t count = 0;
	int i = 0;
	long current_digit = 0;
	long current_number = 0;
	long current_num_digits = 0;
	long denominator = 0;

	if (digits != NULL && array_length > 0 && num_digits > 0) {
		// Return the number of digits that exceed the size of the array
		if (num_digits > array_length) {
			result = num_digits - array_length;
		}
		else {
			current_number = number;
			current_num_digits = num_digits;

			for (i = num_digits - 1; i >= 0; i--) {
				denominator = (long)powl(BASE, i);
				current_digit = get_integer_part_of_quotient(current_number, denominator);
				// Add it to output array
				digits[count++] = current_digit;

				current_number = get_remainder_part_of_quotient(current_number, denominator);
			}

			result = 0;
		}
	}

	return result;
}

static int allocate_digits(size_t num_digits, long ** digits)
{
	int result = 0;

	// Allocate enough memory to hold the number of digits in the number
	int size = sizeof(**digits) * num_digits;
	(*digits) = (long*)malloc(size);

	// Always ensure memory was actually allocated.
	if ((*digits) == NULL) {
		result = 1;
	}
	else {
		// Always initialize arrays.
		memset((*digits), 0, size);
	}

	return result;
}

