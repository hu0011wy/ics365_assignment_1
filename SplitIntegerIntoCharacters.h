/** 
 *  @file SplitIntegerIntoCharacters.h
 *  @brief Function prototypes for splitting a base 10 integer into its digits.
 *  
 *  This contains the prototypes for splitting a base 10 integer between
 *  1 and 32,767 into individual characters and printing them to
 *  the console.
 *
 *  @author Vincent J. Palodichuk
 *  @bug No known bugs.
 */
#pragma once
#ifndef __SPLIT_INTEGER_INTO_CHARACTERS_H__
#define __SPLIT_INTEGER_INTO_CHARACTERS_H__

/**
 *  @brief Returns the integer part of a quotient
 *
 *  Divides the specified numerator by the specified denominator
 *  and returns the integer part of the division. If the denominator
 *  is equal to 0, 0 is returned instead of dividing by 0.
 *
 *  @param numerator the number to be divided
 *  @param denominator the number the numberator is dvided by
 *  @return the integer part of result of the division
 */
long get_integer_part_of_quotient(long numerator, long denominator);

/**
 *  @brief Returns the remainder part of a quotient
 *
 *  Divides the specified numerator by the specified denominator
 *  and returns the remainder part of the division. If the denominator
 *  is equal to 0, 0 is returned instead of dividing by 0.
 *
 *  @param numerator the number to be divided
 *  @param denominator the number the numberator is dvided by
 *  @return the remainder part of result of the division
 */
long get_remainder_part_of_quotient(long numerator, long denominator);

/**
 *  @brief Returns the number of digits in a base 10 integer
 *
 *  Takes the floor of the log10 of the specified base 10 number and
 *  adds 1 to the result to get the number of digits.
 *
 *  @param number the number get the number of digits in

 *  @return the number of digits in the specified number
 */
long get_number_of_digits_in_base10_integer(long number);

/**
 *  @brief Reads an integer from stdin
 *  
 *  Reads an integer from stdin. Returns 0 if invalid input or error.
 *
 *  @return the integer that was read from stdin or 0 invalid input or error.
 */
long read_integer_from_stdin();

/**
 *  @brief Prints the specified string to stdout
 *
 *  Prints the specified array of longs to stdout. The output is formatted such
 *  such that each long is separated by NUMBER_OF_SPACES_BETWEEN_CHARACTERS
 *  space character(s). If the specified string is null, zero length, or empty, the
 *  word 'nothing' is written to stdout. A newline character is always written as the
 *  last character to stdout.
 *
 *  @param output the null terminated string to write to stdout.
 *  @param size the size of the array
 *
 *  @return void
 */
void print_output_message(long *output, size_t size);

/**
 *  @brief Fills the specified array with the digits of the specified number
 *
 *  Fills the specified array, up to array_size, with the digits from the
 *  specified number.
 *
 *  @param number the number that we are getting the digits for
 *
 *  @param num_digits the number of digits in the specified number

 *  @param digits the non null array that we will be filling with
 *         the digits of the number

 *  @param array_length the length of the digits array.
 *
 *  @return 0 if all of the digits of number were placed in the digits array;
 *           otherwise the value is the number of digitis that were truncated.
 */
int get_digits(long number, size_t num_digits, long* digits, size_t array_length);

/*
 *  The smallest valid input integer
 */
extern const long MIN_INPUT_INTEGER;

/*
 *  The largest valid input integer
 */
extern const long MAX_INPUT_INTEGER;

/*
 *  The spaces between characters when printing the results
 *  to stdout
 */
extern const char* SPACES_BETWEEN_CHARACTERS;

/*
 *  The number of times in a row an invalid input value is read from stdin
 *  before giving up and quiting
 */
extern const long MAX_INVALID_INPUT_ATTEMPTS;

#endif // !__SPLIT_INTEGER_INTO_CHARACTERS_H__
